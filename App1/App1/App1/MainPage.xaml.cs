﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        void Handle_Clicked(Object sender, System.EventArgs e)
        {
            double counter = 0;
            double sum = 0;
            double value = 0;
            var numberEntered = new NumberClass();

            UserEntry.Placeholder = "Enter number here";
            UserEntry.BackgroundColor = Color.FromHex("#ffffff");

            Button1.Text = "Clicked";
            //numberEntered.Num = double.Parse(UserEntry.Text);

            if (double.TryParse(UserEntry.Text, out value))
            {
                numberEntered.Num = value;

                while (counter < numberEntered.Num)
                {
                    if ((counter % 3) == 0)
                    {
                        sum = sum + counter;
                    }
                    else if ((counter % 5) == 0)
                    {
                        sum = sum + counter;
                    }
                    counter++;
                }
                Label2.Text = $"The sum of the multiples of 3 and 5 under {numberEntered.Num} is {sum}.";
            }
            else
            {
                UserEntry.Placeholder = "Please enter a valid number";
                UserEntry.BackgroundColor = Color.FromHex("#ff4d4d");
            }

            
        }


    }

}
