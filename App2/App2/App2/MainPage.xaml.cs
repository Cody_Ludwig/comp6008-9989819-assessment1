﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }

        public static int score = 0;

        void Handle_Clicked(Object sender, System.EventArgs e)
        {
            Random rnd = new Random();
            int rndnumber = rnd.Next(1, 10);
            var numberEntered = new UserClass();
            double value = 0;

            UserEntry.Placeholder = "Enter number here";
            UserEntry.BackgroundColor = Color.FromHex("#ffffff");

            if (double.TryParse(UserEntry.Text, out value))
            {
                numberEntered.Num = value;
                Label1.Text = $"{rndnumber}";
                if (numberEntered.Num == rndnumber)

                {                                                                              //result of winning
                    score++;
                    Label1.Text = $"Correct! The number was {rndnumber}";
                    Label2.Text = "";
                    Background.BackgroundColor = Color.FromHex("#33cc33");
                    Background2.BackgroundColor = Color.FromHex("#70db70");
                    Label3.Text = $"Your current score is {score}";
                }

                else                                                                           //result of losing
                {
                    Label1.Text = $"Wrong, The number was {rndnumber}";
                    Label2.Text = "";
                    Background.BackgroundColor = Color.FromHex("#ff0000");
                    Background2.BackgroundColor = Color.FromHex("#ff4d4d");
                    Label3.Text = $"Your current score is {score}";
                }
            }
            else
            {
                UserEntry.Placeholder = "Please enter a valid number";
                UserEntry.BackgroundColor = Color.FromHex("#ff4d4d");
            }
        }
       
    }
    
}